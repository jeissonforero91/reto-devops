# Desarrollo Reto-DevOps CLM
>> Este reto fue diseñado para mostrar tus habilidades DevOps. Este repositorio contiene una aplicación simple en NodeJs.

## El reto comienza aquí

¿Qué pasa con los contenedores? En este momento **(2021)**, los contenedores son un estándar para implementar aplicaciones **(en la nube o en sistemas locales)**. Entonces, el reto es:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
  >> Dockerfile en la raiz del proyecto
2. Ejecutar la app como un usuario diferente de root.
  >> Se configura usuario ´app_user´

Se requiere instalar docker engine de acuerdo a su sistema operativo [install docker Engine](https://docs.docker.com/engine/install/)
**(Las instrucciones para construir o ejecutar el contenedor se encuentran en el Dockerfile.)**


### Reto 2. Docker Compose
![compose](./img/docker-compose.png)

Una vez que haya dockerizado todos los componentes de la API *(aplicación de NodeJS)*, estarás listo para crear un archivo docker-compose, en nuestro equipo solemos usarlo para levantar ambientes de desarrollo antes de empezar a escribir los pipeline. Ya que la aplicación se ejecuta sin ninguna capa para el manejo de protocolo http:

1. Nginx que funcione como proxy reverso a nuesta app Nodejs
  >> Se genera archivo **docker-compose.yaml** con la configuracion correspondiente para levantar un contenedor de aplicacion y uno de proxy con nginx.

2. Asegurar el endpoint /private con auth_basic
  >> Usamos el modulo de [auth_basic](http://nginx.org/en/docs/http/ngx_http_auth_basic_module.html), las credenciales de usuario las manejamos con [htpasswd](https://httpd.apache.org/docs/2.4/programs/htpasswd.html)

3. Habilitar https y redireccionar todo el trafico 80 --> 443
  >> Para la configuracion del servidor nginx es necesario contar con certificados SSL, para esta implementacion se usan certificados autofirmados, en produccion deben ser certificados validos expedidos por una autoridad de certificacion valida.

```bash
        openssl genrsa -out nginx.key 2048
        openssl req -new -key nginx.key -out nginx.csr -subj "/C=XX/ST=XX/L=localhost/O=CLM/CN=localhost"
        openssl x509 -req -days 365 -in nginx.csr -signkey nginx.key -out nginx.crt
}
```
>> Ubicamos el certificado **nginx.crt** y llave generada **nginx.key** en la ruta ``` /nginx/ssl/ ``` configurada en el **docker-compose-yaml** como volumen.
>> El archivo **nginx.csr** lo podemos eliminar, ya que simplemente es la solicitud de firma del certificado.

Se requiere instalar docker [compose](https://docs.docker.com/compose/install/)

Para ejecutar basta con ubicarse en la raiz del proyecto y ejecutar:

``` 
make docker-compose
```

### Reto 3. Probar la aplicación en cualquier sistema CI/CD
![cicd](./img/cicd.jpg)

Como buen ingeniero devops, conoces las ventajas de ejecutar tareas de forma automatizada. Hay algunos sistemas de cicd que pueden usarse para que esto suceda. Elige uno, travis-ci, gitlab-ci, circleci ... lo que quieras. Danos una tubería exitosa. **Gitlab-CI** es nuestra herramienta de uso diario por lo cual obtendras puntos extras si usas gitlab.

 Se generó la configuracion de un pipeline en Gitlab-ci con los siguientes steps:
  - test   --> Ejecuta los test unitarios de la aplicacion.
  - build  --> Construye la imagen de aplicacion y levanta el entorno de trabajo con docker-compose
  - release --> Sube la imagen de aplicacion al registry
  - deploy -->  No implementado

Para el correcto funcionamiento del pipeline es necesario añadir las [variables](https://docs.gitlab.com/ee/ci/variables/index.html) necesarias al pipeline para generar el login al registry y el push de la imagen:

     - CI_REGISTRY_USER       credencial nombre de usuario para login en el registry
     - CI_REGISTRY_PASSWORD   credencial contraseña del usuario para login en el registry
     - CI_CONTAINER_IMAGE     Nombre de la imagen de aplicacion
     - CI_REGISTRY            Nombre del registry para hacer el push de la imagen

  ![pipeline](./img/pipeline.png)


***********************************************************

        (PASOS SIGUIENTES PENDIENTES POR IMPLEMENTAR)
        
***********************************************************


### Reto 4. Deploy en kubernetes  (PENDIENTE POR IMPLEMENTAR)
![k8s](./img/k8s.png)

Ya que eres una máquina creando contenedores, ahora queremos ver tu experiencia en k8s. Use un sistema kubernetes para implementar la API. Recomendamos que uses herramientas como Minikube o Microk8s.

Escriba el archivo de implementación (archivo yaml) utilizalo para implementar su API (aplicación Nodejs).

* añade **Horizontal Pod Autoscaler** a la app NodeJS

### Reto 5. Construir Chart en helm y manejar trafico http(s) (PENDIENTE POR IMPLEMENTAR)
![helm](./img/helm-logo-1.jpg)

Realmente el pan de cada día es crear, modificar y usar charts de helm. Este reto consiste en:

1. Diseñar un chart de helm con nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

### Reto 6. Terraform  (PENDIENTE POR IMPLEMENTAR)
![docker](./img/tf.png)

En estos días en IaC no se habla de más nada que no sea terraform, en **CLM** ya nos encontramos con pipeline automatizados de Iac. El reto consiste en crear un modulo de terraform que nos permita crear un **rbac.authorization de tipo Role** que solo nos permita ver los pods de nuestro **namespace donde se encuentra al app Nodejs**

### Reto 7. Automatiza el despliegue de los retos realizados
![docker](./img/make.gif)

Ya que hoy en día no queremos recordar recetas ni comandos, el reto consiste en **automatizar los retos en un Makefile**, considera especificar cuales son las dependencias necesarias para que tu Makefile se ejecute sin problemas.

