#
# Reto DevOps Node Docker Image
# -----------------------------------
# This file is created for buiding a node docker image for Devops challenge
#
# HOW TO BUILD THIS IMAGE
# -----------------------
# Run:
#      $ docker build -t reto-devops/node:14.18.0-alpine .
#
# HOW TO RUN THIS IMAGE
# -----------------------
# Run:
#      $ docker run --rm -it -d -p 3000:3000 reto-devops/node:14.18.0-alpine
#
# HOW TO TEST THIS IMAGE
# -----------------------
# Run:
#       $ curl -s localhost:3000/ | jq
#           {
#             "msg": "ApiRest prueba"
#           }
# Arguments
# ---------
ARG NODE_VERSION='14.18.0'

# Pull Base Image
# ---------------
FROM node:${NODE_VERSION}-alpine

# Labels
# ------
LABEL version="${NODE_VERSION}-alpine"
LABEL authors="Jeisson Forero <jeisson.forero91@gmail.com>"
LABEL description="This file is created for buiding a node docker image for Devops challenge"

# OS ENV Variables
# ----------------
ENV LANG=en_US.UTF-8 \
    CHARSET=UTF-8 \
    TZ=America/Bogota \
    NODE_ENV=development

RUN adduser app_user --disabled-password
RUN mkdir /app/ && chown -R app_user:app_user /app/
WORKDIR /app/
#COPY --chown=app_user:app_user . .
COPY --chown=app_user:app_user package*.json index.js ./

# Install Dependencies
# --------------------
RUN npm install \
    && npm cache clean --force \
    && rm -rf /tmp/* /var/cache/apk/* ~/.npm/*

EXPOSE 3000
USER app_user
CMD ["node", "index.js"]
